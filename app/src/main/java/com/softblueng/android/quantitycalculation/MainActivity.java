package com.softblueng.android.quantitycalculation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import java.util.*;

import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    int prize = 0;
    int quantity = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }


    public void increment(View view) {

         quantity = quantity+1;
        getval(quantity);
    }

    public void decrement(View view) {
        quantity = quantity-1;
            getval(quantity);

    }
    public void prize(View view) {
        String PrintMessage = "THE COST " ;
        GetMessage(PrintMessage);
        int  prize   =  quantity * 5;
        getprize(prize);

    }


    public void getval(int vn) {
        TextView val = (TextView) findViewById(R.id.quantity);
       val.setText(" "+vn);
    }

    public void  getprize(int pn){
        TextView prize = (TextView) findViewById(R.id.prize);
        prize.setText("N "+pn);
    }
    public  void GetMessage(String message){
        TextView mess = (TextView) findViewById(R.id.message);
        mess.setText(" "+message);
    }
}